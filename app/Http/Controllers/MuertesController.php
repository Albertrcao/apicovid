<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\muertos;
use App\Http\Resources\ShowResponse;
use App\Http\Resources\CovidCollection;
use Illuminate\Support\Facades\DB;

class MuertesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $muertes = new muertes();
        $muertes->fecha = $request->fecha;
        $muertes->id_ccaa = $request->id_ccaa;
        $muertes->incidencia = $request->incidencia;
        $muertes->save();
        return response()->json($muertes);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $muertes = DB::select(DB::raw("select * from muertes where fecha='$id'"));
        if (! $muertes)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$muertes],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showAll()
    {

        $muertos = muertos::all();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $muertos = DB::select(DB::raw("select * from muertos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha.'])],404);
        }

        return new CovidCollection($muertos);

    }

}
