<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')
            ->first();

        $ccaa = ccaas::where('id', $this->ccaas_id)->first();

        if (isset($this->incidencia)) {
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->incidencia,

            ];
        } else {
            return [
                'fecha' => $this->fecha,
                'ccaa' => $ccaa->nombre,
                'pais' => $pais->nombre,
                'value' => $this->numero,
            ];
        }
    }
}
